var adminList=[],roleList=[];
$(function(){
	initRole();
});
function initRole(){
	$.ajax({ 
     type : "get",
     url : BSAE_PATH+"/admin/menu/getRoleList",
     dataType : 'json',
     data : {},
     success : function(result) {
	    if (result.code==200 ) { 
		    roleList=result.data;
		   	initAdmin();
	    }
     } 
    }); 
}

function initAdmin(){
	$.ajax({ 
     type : "get",
     url : BSAE_PATH+"/admin/getAdminList",
     dataType : 'json',
     data : {
     	v : $("#queryKeyWord").val(),
     },
     success : function(result) {
	    if (result.code==200 ) { 
	    	adminList=result.data;
		    var htmlStr='';
		 	for (var i = 0; i < adminList.length; i++) {
			    var admin= adminList[i];
			    htmlStr+='<tr>';
			    htmlStr+='<td>'+admin.name+'</td>';
			    htmlStr+='<td >'+admin.tel+'</td>';
			    htmlStr+='<td>'+getRoleName(admin.roleId)+'</td>';
			    htmlStr+='<td>'+getStateName(admin.state)+'</td>';
			    htmlStr+='<td>'
			       	+'<button type="button" class="btn btn-primary" onclick="addAdminView('+admin.id+')"><i class="icon icon-note"></i>&nbsp; 编辑</button> &nbsp;'
			       	+'<button type="button" class="btn btn-success" onclick="seePassView('+admin.id+')"><i class="icon icon-eye"></i>&nbsp;密码</button> &nbsp;'
			       	+'<button type="button" class="btn btn-danger" onclick="delAdmin('+admin.id+')"><i class="fa fa-trash"></i>&nbsp; 删除</button>'
			       	+'</td>';
				htmlStr+='</tr>';
			}
		    $("#tbodyList").html(htmlStr);
	    }else{ 
	       
	    } 
     } 
    }); 
}
function getRoleName(roleId){
	if(roleId==-1){
		return '<span class="badge badge-danger">超级管理员</span> 【角色锁定】';
	}
	for (var i = 0; i < roleList.length; i++) {
		if(roleId==roleList[i].id){
			return '<span class="badge badge-primary">'+roleList[i].name+'</span>';
		}
		
	}
	return '';
}
function getStateName(state){
	if(state==1){
		return '<span class="badge badge-primary">正常</span>';
	}else if(state==0){
		return '<span class="badge badge-warning">冻结</span>';
	}
	
	return '<span class="badge badge-danger">异常</span>';
}
function delAdmin(id){
	layer.confirm('删除不可恢复，是否确定删除？', {
	  btn: ['确定','取消'] //按钮
	}, function(){
	  layer.msg('模拟删除', {icon: 1});
	}, function(){
	  
	});
}
function saveAdmin(id, name, tel, roleId,pass){
	$.ajax({ 
     type : "post", 
     url : BSAE_PATH+"/admin/saveAdmin",
     dataType : 'json',
     data : { 
     	id : id==-1?null:id,
     	name : name,
     	tel : tel,
     	roleId : roleId,
     	pass :pass
     },
     success : function(result) {
	    if (result.code==200 ) { 
	    	layer.closeAll();
		    initAdmin();
	    }else{ 
	       
	    } 
     } 
    });
}


function addAdminView(id){
	layer.open({
	  type: 1,
	  title : (id==-1)?'新增':'编辑',
	  skin: 'layui-layer-rim', //加上边框
	  area: ['600px', '500px'], //宽高
	  content: '<div class="layerDiv">'
	  +'<div class="form-group"><label for="firstname" class="col-sm-2 control-label">昵称</label><div class="col-sm-12"><input type="text" class="form-control" id="nameInput" placeholder="请输入"></div></div>'
	  +'<div class="form-group"><label for="firstname" class="col-sm-2 control-label">账号</label><div class="col-sm-12"><input type="text" class="form-control" id="telInput" placeholder="请输入" maxlength="11"></div></div>'
	  +'<div class="form-group"><label for="firstname" class="col-sm-2 control-label">密码</label><div class="col-sm-12"><input type="text" class="form-control" id="passInput" placeholder="请输入" maxlength="11"></div></div>'
	  +'<div class="form-group"><label for="firstname" class="col-sm-2 control-label">菜单角色</label><div class="col-sm-12"><select class="form-control" id="roleIdSelect"></select></div></div>'

	 +'</div>',
	 btn: ['确定', '取消'],
	 yes: function(index, layero){
	    
	    var nameInput=$("#nameInput").val();
	    var telInput=$("#telInput").val();
	    var passInput=$("#passInput").val();
	    var roleIdSelect=$("#roleIdSelect").val();
	    if(nameInput.trim()==''){
	    	layer.msg("昵称不能为空");
	    	return;
	    }
	    if(telInput.trim()==''){
	    	layer.msg("账号不能为空");
	    	return;
	    }
	    saveAdmin(id,nameInput, telInput, roleIdSelect,passInput);
	 },
	 success: function(layero, index){
	 	var htmlStr='';
	    var oldObj=null;
		for (var i = 0; i < roleList.length; i++) {
		    var role= roleList[i];
		    htmlStr+='<option value ="'+role.id+'">'+role.name+'</option>';
		}
		$("#roleIdSelect").html(htmlStr);
		for (var i = 0; i < adminList.length; i++) {
		    if(id==adminList[i].id){
		    	oldObj=adminList[i];
		    }
		}
		
		if(oldObj!=null){
			$("#nameInput").val(oldObj.name);
			$("#telInput").val(oldObj.tel);
			$("#passInput").val(oldObj.pass);
			if(oldObj.roleId==-1){
				$("#roleIdSelect").append('<option value ="-1">超级管理员 【锁定】</option>');
				$("#roleIdSelect").prop("disabled","disabled");
			}
			$("#roleIdSelect").val(oldObj.roleId);
		}
		
	 }
	});
}

function seePassView(id){
	layer.open({
	  type: 1,
	  title : '密码',
	  skin: 'layui-layer-rim', //加上边框
	  area: ['600px', '400px'], //宽高
	  content: '<div class="layerDiv">'
	  +'<div class="form-group"><label for="firstname" class="col-sm-2 control-label">昵称:</label><span type="text" class="form-control" id="nameSpan" ></span></div>'
	  +'<div class="form-group"><label for="firstname" class="col-sm-2 control-label">账号:</label><span type="text" class="form-control" id="telSpan" ></span></div>'
	  +'<div class="form-group"><label for="firstname" class="col-sm-2 control-label">密码:</label><span type="text" class="form-control" id="passSpan" ></span></div>'
	 +'</div>',
	 btn: ['确定'],
	 success: function(layero, index){
		for (var i = 0; i < adminList.length; i++) {
		    if(id==adminList[i].id){
		    	oldObj=adminList[i];
		    }
		}
		if(oldObj!=null){
			$("#nameSpan").html(oldObj.name);
			$("#telSpan").html(oldObj.tel);
			$("#passSpan").html(oldObj.pass);
			
		}
		
	 }
	});
}


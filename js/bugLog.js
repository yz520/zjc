var page=0;
var pageSize=10;
$(function(){
    initView();
	initClassList();
});
function initView(){
	 $('#sDate').datetimepicker({
        format: 'YYYY-MM-DD',
        locale: moment.locale('zh-cn')
    });
    
    $('#eDate').datetimepicker({
        format: 'YYYY-MM-DD',
        locale: moment.locale('zh-cn')
    });
}
function initClassList(){
	$.ajax({ 
     type : "get",
     url : BSAE_PATH+"/exceptionLog/getClassList",
     dataType : 'json',
     data : {},
     success : function(result) {
	    if (result.code==200 ) { 
	    	var htmlStr='<option value="-1">全部类(class)</option>';
		   	var data=result.data;
		   	for (var i = 0; i < data.length; i++) {
		   		var obj=data[i];
		   		htmlStr+='<option value="'+obj.className+'">'+obj.className+'</option>';
		   	}
		   	$("#classNameSelect").html(htmlStr);
		   	initExceptionList();
	    }
     } 
    }); 
}

function initExceptionList(){
	$.ajax({ 
     type : "get",
     url : BSAE_PATH+"/exceptionLog/getExceptionList",
     dataType : 'json',
     data : {},
     success : function(result) {
	    if (result.code==200 ) { 
	    	var htmlStr='<option value="-1">全部异常</option>';
		   	var data=result.data;
		   	for (var i = 0; i < data.length; i++) {
		   		var obj=data[i];
		   		htmlStr+='<option value="'+obj.exception+'">'+obj.exception+'</option>';
		   	}
		   	$("#exceptionSelect").html(htmlStr);
		   	
		   	initLogList();
	    }
     } 
    }); 
}

function nextLogList(){
	page++;
	initLogListView();
	
}
function initLogList(){
	page=0;
	initLogListView();
}
function initLogListView(){
	var sort =$("#sortSelect").val();
	var className =$("#classNameSelect").val();
	var exception =$("#exceptionSelect").val();
	var sDate=$("#sDate").val();
	var eDate=$("#eDate").val();
	sDate=(sDate.trim()!='')?(sDate+" 00:00:00"):null;
	eDate=(eDate.trim()!='')?(eDate+" 23:59:59"):null;
 	$.ajax({ 
     type : "get",
     url : BSAE_PATH+"/exceptionLog/getList",
     dataType : 'json',
     data : {
     	page : page,
     	pageSize : pageSize,
     	className : className,
     	exception : exception,
     	sDate : sDate,
     	eDate : eDate,
     	sort : sort
     },
     success : function(result) {
	    if (result.code==200 ) { 
		   	var htmlStr='';
		   	var data=result.data;
		   	for (var i = 0; i < data.length; i++) {
		   		var log=data[i];
		   		htmlStr+='<div class="card-header bg-light">';
							htmlStr+='<div class="list-group-item">';
								htmlStr+='<span class="label label-info log-item">class：</span> '+log.className;
							htmlStr+='</div>';
							htmlStr+='<div class="list-group-item">';
								htmlStr+='<span class="label label-info log-item">异常：</span> '+log.exception;
							htmlStr+='</div>';
							htmlStr+='<div class="list-group-item">';
								htmlStr+='<span class="label label-info log-item">方法：</span> '+log.methodName;
							htmlStr+='</div>';
							htmlStr+='<div class="list-group-item">';
								htmlStr+='<span class="label label-info log-item">参数：</span> '+log.args;
							htmlStr+='</div>';
							htmlStr+='<div class="list-group-item">';
								htmlStr+='<span class="label label-info log-item">时间：</span> '+dateFtt("yyyy-MM-dd hh:mm:ss",new Date(log.createTime));
							htmlStr+='</div>';
							
							htmlStr+='<div class="ist-group-item" style="margin: 3px 0px 0px 0px;">';
								htmlStr+='<button type="button" class="btn btn-default btn-lg btn-block"  data-toggle="collapse" data-parent="#accordion"  href="#collapse_'+page+'_'+i+'">点击查看详('+(page*pageSize+i+1)+')</button>';
							htmlStr+='</div>';
							htmlStr+='<div id="collapse_'+page+'_'+i+'" class="panel-collapse collapse in">';
								htmlStr+='<pre  class="codePre">'+(log.text.replace(new RegExp("<","g"),'《').replace(new RegExp(">","g"),'》'))+'</pre>';
							htmlStr+='</div>';
							
					htmlStr+='</div>';
		   	}
		   	if(page==0){
		   		$("#bugListDiv").html(htmlStr);
		   	}else{
		   		$("#bugListDiv").append(htmlStr);
		   		
		   	}
		   	if(data.length<pageSize){
		   		//结束
		   		$("#nextButton").hide();
		   	}else{
		   		//未结束
		   		$("#nextButton").show();
		   	}
		   	
	    }
     } 
    }); 
}

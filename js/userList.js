var pageNum = 1;
var pageSize = 10;
var totalPage = 1;
$(function() {
	initUser();
});

function initPage() {
	$("#pagination1").pagination({
		currentPage: pageNum,
		totalPage: totalPage,
		callback: function(current) {
			pageNum = current;
			initUser();
		}
	});
}

function initUser() {
	$.ajax({
		type: "get",
		url: BSAE_PATH + "/admin/user/getList",
		dataType: 'json',
		data: {
			"key": $("#queryKeyWord").val(),
			"state": $("#stateSelect").val(),
			pageNum: pageNum,
			pageSize: pageSize,
		},
		success: function(result) {
			if(result.code == 200) {
				list = result.data.list;
				totalPage = result.data.pages;
				initPage();
				var htmlStr = '';
				for(var i = 0; i < list.length; i++) {
					var user = list[i];
					htmlStr += '<tr>';
					htmlStr += '<td >' + (i - 0 + 1) + '</td>';
					htmlStr += '<td >' + user.name + '</td>';
					htmlStr += '<td >' + user.tel + '</td>';
					htmlStr += '<td >' + user.pass + '</td>';
					htmlStr += '<td >' + user.gymName + '</td>';
					htmlStr += '<td >' + ((user.sex == 1) ? '男' : '女') + '</td>';
					htmlStr += '<td >' + fmtDate(user.addTime) + '</td>';
					htmlStr += '<td >' + getStateName(user.state, user.id) + '</td>';
					htmlStr += '<td >' +
						'<button type="button" class="btn btn-primary" onclick="updateUserView(' + user.id + ',\'' + user.name + '\',\'' + user.tel + '\',\'' + user.pass + '\')"><i class="icon icon-note"></i>&nbsp; 修改</button> &nbsp;' +
						'</td>';
					htmlStr += '</tr>';
				}
				$("#tbodyList").html(htmlStr);
			} else {

			}
		}
	});
}

function getStateName(state, id) {
	if(state == 1) {
		return '<button type="button" class="btn btn-success" onclick="changeStateView(' + id + ',0)"><i class="icon icon-check"></i>&nbsp; 正常</button>';
	} else if(state == 0) {
		return '<button type="button" class="btn btn-danger" onclick="changeStateView(' + id + ',1)"><i class="icon icon-close"></i>&nbsp; 冻结</button>';
	}

	return '<span class="badge badge-danger">异常</span>';
}

function changeStateView(id, state) {
	layer.confirm(state == 0 ? '是否冻结账号？' : '是否解封账号？', {
		btn: ['确定', '取消'] //按钮
	}, function() {
		changeState(id, state);
	}, function() {

	});
}

function changeState(id, state) {
	$.ajax({
		type: "post",
		url: BSAE_PATH + "/admin/user/update",
		dataType: 'json',
		data: {
			id: id,
			state: state
		},
		success: function(result) {
			layer.msg(result.msg);
			if(result.code == 200) {
				initUser();
			}
		}
	});
}

function updateUser(id, name, tel, pass) {
	$.ajax({
		type: "post",
		url: BSAE_PATH + "/admin/user/update",
		dataType: 'json',
		data: {
			id: id,
			name: name,
			tel: tel,
			pass: pass
		},
		success: function(result) {
			if(result.code == 200) {
				layer.closeAll();
				layer.msg(result.msg);
				initUser();
			} else {
				layer.msg(result.msg);
			}
		}
	});
}

function updateUserView(id, name, tel,pass) {
	layer.open({
		type: 1,
		title: '修改',
		skin: 'layui-layer-rim', //加上边框
		area: ['600px', '400px'], //宽高
		content: '<div class="layerDiv">' +
			'<div class="form-group"><label for="firstname" class="col-sm-12 control-label">昵称</label><div class="col-sm-12"><input type="text" class="form-control" id="nameInput" placeholder="请输入"></div></div>' +
			'<div class="form-group"><label for="firstname" class="col-sm-12 control-label">手机号</label><div class="col-sm-12"><input type="text" class="form-control" id="telInput" placeholder="请输入"></div></div>' +
			'<div class="form-group"><label for="firstname" class="col-sm-12 control-label">密码</label><div class="col-sm-12"><input type="text" class="form-control" id="passInput" placeholder="请输入"></div></div>' +
			'</div>',
		btn: ['确定', '取消'],
		yes: function(index, layero) {
			var nameInput = $("#nameInput").val();
			var telInput = $("#telInput").val();
			var passInput = $("#passInput").val();
			if(nameInput.trim() == '') {
				layer.msg("昵称不能为空");
				return;
			}
			if(telInput.trim() == '') {
				layer.msg("手机号不能为空");
				return;
			}
			if(passInput.trim() == '') {
				layer.msg("手机号不能为空");
				return;
			}

			updateUser(id, nameInput, telInput,passInput);
		},
		success: function(layero, index) {
			$("#nameInput").val(name);
			$("#telInput").val(tel);
			$("#passInput").val(pass);
		}
	});
}
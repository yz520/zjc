var menuList=[];
$(function(){
	initMenu();
});
function initMenu(){
	$.ajax({ 
     type : "get",
     url : BSAE_PATH+"/admin/menu/getMenuTree",
     dataType : 'json',
     data : {},
     success : function(result) {
	    if (result.code==200 ) { 
		    var htmlStr='';
		    htmlStr+=getMenuHtmlStr(result.data,0);
		    $("#tbodyList").html(htmlStr);
	    }else{ 
	       
	    } 
     } 
    }); 
}
	var levelrgb=["	#FFFFF0","#FFFFE0","#FFF8DC"];
	function getMenuHtmlStr(list,level){
		var htmlStr='';
	 	for (var i = 0; i < list.length; i++) {
		    var menu= list[i];
		    menuList[menuList.length]=menu;
		    htmlStr+='<tr style="background:'+levelrgb[level%levelrgb.length]+';">';
		    htmlStr+='<td><i class="icon '+menu.icon+'"></i></td>';
		    htmlStr+='<td style="padding-left:'+(level*30)+'px">'+menu.name+'</td>';
		    htmlStr+='<td>'+menu.value+'</td>';
		    htmlStr+='<td>'+menu.text+'</td>';
		    htmlStr+='<td style="color:red;">'+getMenuName(menu.pid)+'</td>';
		    htmlStr+='<td>'
		       	+'<button type="button" class="btn btn-primary" onclick="addMenuView('+menu.id+')"><i class="icon icon-note"></i> &nbsp; 编辑</button> &nbsp;'
		       	+'<button type="button" class="btn btn-danger" onclick="delMenu('+menu.id+')"><i class="fa fa-trash"></i> &nbsp; 删除</button>'
		       	+'</td>';
			htmlStr+='</tr>';
			if(menu.list!=undefined&&menu.list!=null&&menu.list.length>0){
				htmlStr+=getMenuHtmlStr(menu.list,(level+1));
			}
		}
	 return htmlStr;
	}
function getMenuName(id){
	if(id==-1){
		return '';
	}
	for (var i = 0; i < menuList.length; i++) {
		if(menuList[i].id==id){
			return menuList[i].name;
		}
	}
}
function delMenu(id){
	layer.confirm('删除不可恢复，是否确定删除？', {
	  btn: ['确定','取消'] //按钮
	}, function(){
	  layer.msg('模拟删除', {icon: 1});
	}, function(){
	  
	});
}
function saveMenu(id, pid, name, text, value, icon, type){
	$.ajax({ 
     type : "post", 
     url : BSAE_PATH+"/admin/menu/saveMenu",
     dataType : 'json',
     data : { 
     	id : id,
     	pid : pid,
     	name : name,
     	text : text,
     	value : value,
     	icon : icon,
     	type : type,
     },
     success : function(result) {
	    if (result.code==200 ) { 
	    	layer.closeAll();
		    initMenu();
	    }else{ 
	       
	    } 
     } 
    });
}


function addMenuView(id){
	layer.open({
	  type: 1,
	  title : (id==-1)?'新增':'编辑',
	  skin: 'layui-layer-rim', //加上边框
	  area: ['600px', '600px'], //宽高
	  content: '<div class="layerDiv">'
	  +'<div class="form-group"><label for="firstname" class="col-sm-2 control-label">图标: <span id="iconImg" style="font-size:20px;"></span></label><div class="col-sm-12"><div class="rootBar" id="iconDiv"></div></div></div>'
	  +'<div class="form-group"><label for="firstname" class="col-sm-2 control-label">菜单名</label><div class="col-sm-12"><input type="text" class="form-control" id="nameInput" placeholder="请输入"></div></div>'
	  +'<div class="form-group"><label for="firstname" class="col-sm-2 control-label">url</label><div class="col-sm-12"><input type="text" class="form-control" id="valueInput" placeholder="请输入"></div></div>'
	  +'<div class="form-group"><label for="firstname" class="col-sm-2 control-label">备注</label><div class="col-sm-12"><input type="text" class="form-control" id="textInput" placeholder="请输入"></div></div>'
	  +'<div class="form-group"><label for="firstname" class="col-sm-2 control-label">上一级</label><div class="col-sm-12"><select class="form-control" id="pidSelect"></select></div></div>'

	 +'</div>',
	 btn: ['确定', '取消'],
	 yes: function(index, layero){
	    var icon=$("input[name='optionsRadiosinline']:checked").val();
	    if(icon==undefined){
	    	layer.msg("请选择图标");
	    	return;
	    }
	    var nameInput=$("#nameInput").val();
	    var valueInput=$("#valueInput").val();
	    var textInput=$("#textInput").val();
	    var pidSelect=$("#pidSelect").val();
	    if(nameInput.trim()==''){
	    	layer.msg("菜单名不能为空");
	    	return;
	    }
	    if(pidSelect==null||pidSelect==undefined){
	    	pidSelect=-1;
	    }
	    saveMenu(id,pidSelect, nameInput, textInput, valueInput, icon, 1);
	 },
	 success: function(layero, index){
	 	var htmlStr='';
		for (var i = 0; i < iconList.length; i++) {
			var icon=iconList[i];
			htmlStr+='<label class="childrenBar radio-inline"><input type="radio" name="optionsRadiosinline" id="optionsRadios_'+icon+'" value="'+icon+'" ><i class="icon '+icon+'"></i></label>';
		}
	    $("#iconDiv").html(htmlStr);
	    htmlStr='<option value ="-1">根目录</option>';
	    var oldMenu=null;
		for (var i = 0; i < menuList.length; i++) {
		    var menu= menuList[i];
		    if(menu.pid==-1){
		    	htmlStr+='<option value ="'+menu.id+'">'+menu.name+'</option>';
		    }else{
		    	htmlStr+='<option value ="'+menu.id+'">'+menu.name+'</option>';
		    }
		    if(id==menu.id){
		    	oldMenu=menu;
		    }
		}
		$("#pidSelect").html(htmlStr);
		if(oldMenu!=null){
			$("#nameInput").val(oldMenu.name);
			$("#valueInput").val(oldMenu.value);
			$("#textInput").val(oldMenu.text);
			$("#pidSelect").val(oldMenu.pid);
			$("#iconImg").html('<i class="icon '+oldMenu.icon+'"></i>');
			
			$(":radio[name='optionsRadiosinline'][value='" + oldMenu.icon + "']").prop("checked", "checked");
			
		}
		
	 }
	});
}

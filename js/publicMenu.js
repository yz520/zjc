var admin=null;
$(document).ready(function () {
	initTopMenu();
	initAdminInfo();
	
});
function goLogin(){
	location.href="login.html"
}
function goPassSet(){
	location.href="passSet.html"
}
function initAdminInfo(){
	$.ajax({ 
	     type : "get",
	     url : BSAE_PATH+"/admin/getAdminInfo",
	     dataType : 'json',
	     data : {},
	     xhrFields:{ withCredentials:true},
	     success : function(result) {
		    if (result.code==200 ) { 
			    admin=result.data;
			    if(admin.menuList!=null&&admin.menuList!=undefined){
			    	leftMenuList=admin.menuList;			    	
				   	initLeftMenu();
					initSidebarToggle();
			    }
			    $(".adminName").html(admin.name);
			    
		    }else{ 
		       goLogin();
		    } 
	     } ,
	     error: function(){
			 goLogin();
	     }
    }); 
}
function initTopMenu(){
	var htmlStr='';
	htmlStr+='<a href="#" class="btn btn-link sidebar-mobile-toggle d-md-none mr-auto"><i class="fa fa-bars"></i></a>';
    htmlStr+='<a class="navbar-brand" href="#">健身房小程序管理系统</a>';
    htmlStr+='<a href="#" class="btn btn-link sidebar-toggle d-md-down-none"><i class="fa fa-bars"></i></a>';
    htmlStr+='<ul class="navbar-nav ml-auto">';
    htmlStr+='<li class="nav-item d-md-down-none"><a href="#"><i class="fa fa-envelope-open"></i> <span class="badge badge-pill badge-danger">0</span></a></li>';

    htmlStr+='<li class="nav-item dropdown">';
    htmlStr+='<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
    htmlStr+='<img src="./imgs/avatar-1.png" class="avatar avatar-sm" alt="logo">';
    htmlStr+='<span class="small ml-1 d-md-down-none adminName"></span>';
    htmlStr+='</a>';
    
    htmlStr+='<div class="dropdown-menu dropdown-menu-right">';
    htmlStr+='<a href="#" class="dropdown-item" onclick="goPassSet()"><i class="fa fa-wrench"></i>密码设置</a>';
    htmlStr+='<a href="#" class="dropdown-item" onclick="loginOut()"><i class="fa fa-lock"></i> 安全退出</a>';
    htmlStr+='</div>';
    
    htmlStr+='</li>';
    htmlStr+='</ul>';
    $(".top-menu").html(htmlStr);
}
var leftMenuList=[];
function initLeftMenu(){
	var obj=getMenuObj(leftMenuList,0)
	$(".left-menu").html(obj.htmlStr);
}

function getMenuObj(list,level){
	var obj={
		isClick : false,
		htmlStr : '',
		clickName : ''
	}
	var htmlStr='';
	htmlStr+='<div class="left-menu-item left-menu-item-level-'+level+' ">'
	for (var i = 0; i <list.length; i++) {
    	var menu= list[i];
    	if(menu.list==undefined||menu.list==null||menu.list.length==0){
    		var isCurrent =isCurrentUrl(menu.value);
    		obj.isClick=obj.isClick||isCurrent;
    		htmlStr+='<div onclick="goMenu(\''+menu.value+'\')" class="left-menu-item-text '+(isCurrent?'active':'')+'"><i class="icon '+menu.icon+'"></i> '+menu.name+'</a></div>';
    	}else{
    		var childObj=getMenuObj(menu.list,level+1);
    		obj.isClick=obj.isClick||childObj.isClick;
    		htmlStr+='<div class="left-menu-item-text left-menu-item-list '+(childObj.isClick?'active open':'')+' "><i class="icon '+menu.icon+'"></i> '+menu.name+'</a></div>';
    		htmlStr+=childObj.htmlStr;
    	}
    	
    }
	htmlStr+='</div>';
	obj.htmlStr=htmlStr;
	return obj;
}
function loginOut(){
	$.ajax({ 
	     type : "get",
	     url : BSAE_PATH+"/admin/loginOut",
	     dataType : 'json',
	     data : {},
	     xhrFields:{ withCredentials:true},
	     success : function(result) {
		    if (result.code==200 ) { 
		    	goLogin();
		    }else{ 
		       goLogin();
		    } 
	     },
	     error: function(){
			 goLogin();
	     }
    }); 
	
}

/**
 * 初始化菜单显示隐藏
 */
function initSidebarToggle(){
	/**
     * Sidebar Dropdown
     */
    $('.nav-dropdown-toggle').on('click', function (e) {
        e.preventDefault();
        $(this).parent().toggleClass('open');
    });

    // open sub-menu when an item is active.
    $('ul.nav').find('a.active').parent().parent().parent().addClass('open');

    /**
     * Sidebar Toggle
     */
    $('.sidebar-toggle').on('click', function (e) {
        e.preventDefault();
        $('body').toggleClass('sidebar-hidden');
    });

    /**
     * Mobile Sidebar Toggle
     */
    $('.sidebar-mobile-toggle').on('click', function () {
        $('body').toggleClass('sidebar-mobile-show');
    });
    
    $('.left-menu-item-list').on('click', function (e) {
       if($(this).hasClass("open")){
       		$(this).removeClass("open");
       		$(this).next().slideUp(300);
       }else{
       		$(this).addClass("open");
       		$(this).next().slideDown(300);
       }
    });
    //隐藏所有未选择的子菜单
    var list=$('.left-menu-item-list');
    for (var i = 0; i < list.length; i++) {
    	var obj=list[i];
    	if(!$(obj).hasClass("open")){
    		$(obj).next().hide();
    	}
    }

}

function isCurrentUrl(menuUrl){
	if(menuUrl==undefined||menuUrl==null||menuUrl.trim()==''){
		return false;
	}
	if (window.location.href.split(menuUrl).length>1) {
		return true;
	}else{
		return false;
	}

}

function goMenu(menuUrl){
	if(menuUrl==undefined||menuUrl==null||menuUrl.trim()==''){
		return false;
	}
	window.location.href=menuUrl;
}

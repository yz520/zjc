var pageNum = 1;
var pageSize = 10;
var totalPage = 1;
$(function() {
	initGym();
});

function initPage() {
	$("#pagination1").pagination({
		currentPage: pageNum,
		totalPage: totalPage,
		callback: function(current) {
			pageNum = current;
			initGym();
		}
	});
}

function initGym() {
	$.ajax({
		type: "get",
		url: BSAE_PATH + "/admin/gym/getList",
		dataType: 'json',
		data: {
			"key": $("#queryKeyWord").val(),
			"state": $("#stateSelect").val(),
			pageNum: pageNum,
			pageSize: pageSize,
		},
		success: function(result) {
			if(result.code == 200) {
				list = result.data.list;
				totalPage = result.data.pages;
				initPage();
				var htmlStr = '';
				for(var i = 0; i < list.length; i++) {
					var gym = list[i];
					htmlStr += '<tr>';
					htmlStr += '<td >' + (i - 0 + 1) + '</td>';
					htmlStr += '<td >' + gym.name + '</td>';
					htmlStr += '<td > <button type="button" class="btn btn-success" onclick="manageListView(' + gym.id + ')"> ' + gym.manage + ' </button> <button type="button" class="btn btn-primary" onclick="addManageView(' + gym.id + ')"><i class="icon icon-plus"></i></button></td>';
					htmlStr += '<td >' + gym.coach + '</td>';
					htmlStr += '<td >' + gym.student + '</td>';
					htmlStr += '<td >' + fmtDate(gym.endTime) + '</td>';
					htmlStr += '<td >' + gym.maxSize + '</td>';
					htmlStr += '<td >' + getStateName(gym.state, gym.id) + '</td>';
					htmlStr += '<td >' +
						'<button type="button" class="btn btn-primary" onclick="updateGymView(' + gym.id + ',\'' + gym.name + '\',' + gym.endTime + ',' + gym.maxSize + ')"><i class="icon icon-note"></i>&nbsp; 修改</button> &nbsp;' +
						'</td>';
					htmlStr += '</tr>';
				}
				$("#tbodyList").html(htmlStr);
			} else {

			}
		}
	});
}

function getStateName(state, id) {
	if(state == 1) {
		return '<button type="button" class="btn btn-success" onclick="changeStateView(' + id + ',0)"><i class="icon icon-check"></i>&nbsp; 正常</button>';
	} else if(state == 0) {
		return '<button type="button" class="btn btn-danger" onclick="changeStateView(' + id + ',1)"><i class="icon icon-close"></i>&nbsp; 冻结</button>';
	}

	return '<span class="badge badge-danger">异常</span>';
}

function changeStateView(id, state) {
	layer.confirm(state == 0 ? '是否冻结账号？' : '是否解封账号？', {
		btn: ['确定', '取消'] //按钮
	}, function() {
		changeState(id, state);
	}, function() {

	});
}

function changeState(id, state) {
	$.ajax({
		type: "post",
		url: BSAE_PATH + "/admin/gym/update",
		dataType: 'json',
		data: {
			id: id,
			state: state
		},
		success: function(result) {
			layer.msg(result.msg);
			if(result.code == 200) {
				initGym();
			}
		}
	});
}

function addGym(name, tel, manageName, endTime, maxSize) {
	$.ajax({
		type: "post",
		url: BSAE_PATH + "/admin/gym/add",
		dataType: 'json',
		data: {
			name: name,
			tel: tel,
			manageName: manageName,
			endTime: endTime,
			maxSize: maxSize
		},
		success: function(result) {
			if(result.code == 200) {
				layer.closeAll();
				layer.msg(result.msg);
				initGym();
			} else {
				layer.msg(result.msg);
			}
		}
	});
}

function addGymView() {
	layer.open({
		type: 1,
		title: '新增',
		skin: 'layui-layer-rim', //加上边框
		area: ['600px', '400px'], //宽高
		content: '<div class="layerDiv">' +
			'<div class="form-group"><label for="firstname" class="col-sm-12 control-label">健身房名字</label><div class="col-sm-12"><input type="text" class="form-control" id="nameInput" placeholder="请输入"></div></div>' +
			'<div class="form-group"><label for="firstname" class="col-sm-12 control-label">管理员昵称</label><div class="col-sm-12"><input type="text" class="form-control" id="manageNameInput" placeholder="请输入" maxlength="11"></div></div>' +
			'<div class="form-group"><label for="firstname" class="col-sm-12 control-label">管理员手机号</label><div class="col-sm-12"><input type="text" class="form-control" id="telInput" placeholder="请输入" maxlength="11"></div></div>' +
			'<div class="form-group"><label for="firstname" class="col-sm-12 control-label">到期时间</label><div class="col-sm-12"><input type="text" class="form-control" id="endTimeInput" placeholder="请输入" ></div></div>' +
			'<div class="form-group"><label for="firstname" class="col-sm-12 control-label">限制人数</label><div class="col-sm-12"><input type="text" class="form-control" id="maxSizeInput" placeholder="请输入" maxlength="11"></div></div>' +
			'</div>',
		btn: ['确定', '取消'],
		yes: function(index, layero) {

			var nameInput = $("#nameInput").val();
			var telInput = $("#telInput").val();
			var manageNameInput = $("#manageNameInput").val();
			var endTimeInput = $("#endTimeInput").val();
			var maxSizeInput = $("#maxSizeInput").val();
			if(nameInput.trim() == '') {
				layer.msg("健身房名字不能为空");
				return;
			}
			if(telInput.trim() == '') {
				layer.msg("管理员手机号不能为空");
				return;
			}
			if(manageNameInput.trim() == '') {
				layer.msg("管理员昵称不能为空");
				return;
			}
			addGym(nameInput, telInput, manageNameInput,endTimeInput,maxSizeInput);
		},
		success: function(layero, index) {}
	});
}

function updateGym(id, name, endTime, maxSize) {
	$.ajax({
		type: "post",
		url: BSAE_PATH + "/admin/gym/update",
		dataType: 'json',
		data: {
			id: id,
			name: name,
			endTime: endTime,
			maxSize: maxSize
		},
		success: function(result) {
			if(result.code == 200) {
				layer.closeAll();
				layer.msg(result.msg);
				initGym();
			} else {
				layer.msg(result.msg);
			}
		}
	});
}

function updateGymView(id, name, endTime, maxSize) {
	layer.open({
		type: 1,
		title: '修改',
		skin: 'layui-layer-rim', //加上边框
		area: ['600px', '400px'], //宽高
		content: '<div class="layerDiv">' +
			'<div class="form-group"><label for="firstname" class="col-sm-12 control-label">健身房名字</label><div class="col-sm-12"><input type="text" class="form-control" id="nameInput" placeholder="请输入"></div></div>' +
			'<div class="form-group"><label for="firstname" class="col-sm-12 control-label">到期时间</label><div class="col-sm-12"><input type="text" class="form-control" id="endTimeInput" placeholder="请输入" ></div></div>' +
			'<div class="form-group"><label for="firstname" class="col-sm-12 control-label">限制人数</label><div class="col-sm-12"><input type="text" class="form-control" id="maxSizeInput" placeholder="请输入" maxlength="11"></div></div>' +
			'</div>',
		btn: ['确定', '取消'],
		yes: function(index, layero) {
			var nameInput = $("#nameInput").val();
			var endTimeInput = $("#endTimeInput").val();
			var maxSizeInput = $("#maxSizeInput").val();
			if(nameInput.trim() == '') {
				layer.msg("健身房名字不能为空");
				return;
			}
			updateGym(id, nameInput,endTimeInput,maxSizeInput);
		},
		success: function(layero, index) {
			$("#nameInput").val(name);
			if(endTime==undefined||endTime==null){
				$("#endTimeInput").val(fmtDate(new Date()));
			}else{
				$("#endTimeInput").val(fmtDate(endTime));
			}
			$("#maxSizeInput").val(maxSize);
		}
	});
}

function addManage(id, tel, manageName) {
	$.ajax({
		type: "post",
		url: BSAE_PATH + "/admin/gym/addManage",
		dataType: 'json',
		data: {
			id: id,
			tel: tel,
			manageName: manageName
		},
		success: function(result) {
			if(result.code == 200) {
				layer.closeAll();
				layer.msg(result.msg);
				initGym();
			} else {
				layer.msg(result.msg);
			}
		}
	});
}

function addManageView(id) {
	layer.open({
		type: 1,
		title: '新增',
		skin: 'layui-layer-rim', //加上边框
		area: ['600px', '310px'], //宽高
		content: '<div class="layerDiv">' +
			'<div class="form-group"><label for="firstname" class="col-sm-12 control-label">管理员昵称</label><div class="col-sm-12"><input type="text" class="form-control" id="manageNameInput" placeholder="请输入" maxlength="11"></div></div>' +
			'<div class="form-group"><label for="firstname" class="col-sm-12 control-label">管理员手机号</label><div class="col-sm-12"><input type="text" class="form-control" id="telInput" placeholder="请输入" maxlength="11"></div></div>' +
			'</div>',
		btn: ['确定', '取消'],
		yes: function(index, layero) {
			var telInput = $("#telInput").val();
			var manageNameInput = $("#manageNameInput").val();

			if(telInput.trim() == '') {
				layer.msg("管理员手机号不能为空");
				return;
			}
			if(manageNameInput.trim() == '') {
				layer.msg("管理员昵称不能为空");
				return;
			}
			addManage(id, telInput, manageNameInput);
		},
		success: function(layero, index) {}
	});
}

function manageListView(id) {
	layer.open({
		type: 1,
		title: '管理员',
		skin: 'layui-layer-rim', //加上边框
		area: ['800px', '400px'], //宽高
		content: '<div class="layerDiv">' +
			'<table class="table table-bordered">' +
			'<thead><tr><th>序号</th><th>手机号</th><th>管理员</th><th>用户昵称</th><th>状态</th><th>操作</th></tr></thead>' +
			'<tbody id="tbodyManageList"></tbody>' +
			'</table>' +
			'</div>',
		btn: ['确定'],
		success: function(layero, index) {
			initManage(id)
		}
	});
}

function initManage(id) {
	$.ajax({
		type: "get",
		url: BSAE_PATH + "/admin/gym/getManageList",
		dataType: 'json',
		data: {
			"id": id
		},
		success: function(result) {
			if(result.code == 200) {
				list = result.data;
				var htmlStr = '';
				for(var i = 0; i < list.length; i++) {
					var manage = list[i];
					htmlStr += '<tr>';
					htmlStr += '<td >' + (i - 0 + 1) + '</td>';
					htmlStr += '<td >' + manage.tel + '</td>';
					htmlStr += '<td >' + manage.name + '</td>';
					htmlStr += '<td >' + manage.userName + '</td>';
					htmlStr += '<td >' + getManageStateName(id, manage.state, manage.id) + '</td>';
					htmlStr += '<td >' +
						'<button type="button" class="btn btn-primary" onclick="updateManageView(' + id + ',' + manage.id + ',\'' + manage.name + '\')"><i class="icon icon-note"></i>&nbsp; 修改</button> &nbsp;' +
						'</td>';
					htmlStr += '</tr>';
				}
				$("#tbodyManageList").html(htmlStr);
			} else {

			}
		}
	});
}

function updateManage(gymId, id, name, index) {
	$.ajax({
		type: "post",
		url: BSAE_PATH + "/admin/gym/updateManage",
		dataType: 'json',
		data: {
			id: id,
			name: name
		},
		success: function(result) {
			if(result.code == 200) {
				layer.close(index);
				layer.msg(result.msg);
				initManage(gymId);
			} else {
				layer.msg(result.msg);
			}
		}
	});
}

function updateManageView(gymId, id, name) {
	layer.open({
		type: 1,
		title: '修改',
		skin: 'layui-layer-rim', //加上边框
		area: ['600px', '250px'], //宽高
		content: '<div class="layerDiv">' +
			'<div class="form-group"><label for="firstname" class="col-sm-12 control-label">管理员名字</label><div class="col-sm-12"><input type="text" class="form-control" id="nameInput" placeholder="请输入"></div></div>' +
			'</div>',
		btn: ['确定', '取消'],
		yes: function(index, layero) {
			var nameInput = $("#nameInput").val();
			if(nameInput.trim() == '') {
				layer.msg("管理员名字不能为空");
				return;
			}
			updateManage(gymId, id, nameInput, index);
		},
		success: function(layero, index) {
			$("#nameInput").val(name);
		}
	});
}

function getManageStateName(gymId, state, id) {
	if(state == 1) {
		return '<button type="button" class="btn btn-success" onclick="changeManageStateView(' + gymId + ',' + id + ',0)"><i class="icon icon-check"></i>&nbsp; 正常</button>';
	} else if(state == 0) {
		return '<button type="button" class="btn btn-danger" onclick="changeManageStateView(' + gymId + ',' + id + ',1)"><i class="icon icon-close"></i>&nbsp; 冻结</button>';
	}

	return '<span class="badge badge-danger">异常</span>';
}

function changeManageStateView(gymId, id, state) {
	layer.confirm(state == 0 ? '是否冻结账号？' : '是否解封账号？', {
		btn: ['确定', '取消'] //按钮
	}, function() {
		changeManageState(gymId, id, state);
	}, function() {

	});
}

function changeManageState(gymId, id, state) {
	$.ajax({
		type: "post",
		url: BSAE_PATH + "/admin/gym/updateManage",
		dataType: 'json',
		data: {
			id: id,
			state: state
		},
		success: function(result) {
			layer.msg(result.msg);
			if(result.code == 200) {
				initManage(gymId);
			}
		}
	});
}
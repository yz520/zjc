package com.zz.zjc;

import com.zz.zjc.dao.PhrasesDao;
import com.zz.zjc.model.Phrases;
import net.sourceforge.pinyin4j.PinyinHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

/**
 * @author 诸杰成
 * @description
 * @date 2020/7/25 13:50
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ChengYu {

    @Autowired
    PhrasesDao phrasesDao;

    @Test
    public void make() {
        List<Phrases> list = phrasesDao.selectAll();
        for (int i = 0; i < list.size(); i++) {
            System.out.println("===== "+ i);

            Phrases phrases =list.get(i);
            try {
                String text_ = phrases.getChengyu().substring(0,1);
                String[] pinyin = PinyinHelper.toHanyuPinyinStringArray(text_.charAt(0)); // str.charAt(0) 第一个汉字
                if(pinyin.length>0){
                    phrases.setPingyinfirst2(pinyin[0]);
                    phrasesDao.updateByPrimaryKey(phrases);
                }
            }catch (Exception e){

            }


        }
    }
}

package com.zz.zjc.vo;

import com.zz.zjc.model.Admin;
import com.zz.zjc.model.Menu;
import io.swagger.annotations.ApiModel;

import java.util.List;

/**
 * @author 诸杰成
 * @description
 * @date 2020/4/3 17:14
 */
@ApiModel("管理员信息")
public class AdminVo extends Admin {

    private List<Menu> menuList;

    public List<Menu> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<Menu> menuList) {
        this.menuList = menuList;
    }

    public AdminVo() {
    }
}

package com.zz.zjc.exception;

import com.zz.zjc.config.ResultCode;

/**
 * 自定义异常处理
 */
public class MessageException extends RuntimeException {
    private String msg;
    private int code;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
    public MessageException(ResultCode resultCode) {
        this.msg = resultCode.getMsg();
        this.code = resultCode.getCode();
    }

    public MessageException(int code,String msg) {
        this.msg = msg;
        this.code = code;
    }
    public MessageException(String msg) {
        this.msg = msg;
        this.code = ResultCode.CODE_ERROR_MESSAGE_EXCEPTION.getCode();
    }
}

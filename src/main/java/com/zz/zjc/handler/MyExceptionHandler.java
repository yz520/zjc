package com.zz.zjc.handler;

import com.zz.zjc.config.ResultCode;
import com.zz.zjc.exception.MessageException;
import com.zz.zjc.util.ResultUtil;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class MyExceptionHandler {

    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public String javaExceptionHandler(Exception ex) {
        //返回默认的系统异常失败
        return ResultUtil.fail(ResultCode.CODE_ERROR);
    }


    @ResponseBody
    @ExceptionHandler(value = MessageException.class)
    public String messageExceptionnHandler(MessageException ex) {
        //MessageException是自定义的一个异常
        return ResultUtil.fail(ex.getCode(),ex.getMsg());
    }
}

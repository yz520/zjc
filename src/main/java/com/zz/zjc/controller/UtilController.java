package com.zz.zjc.controller;

import com.zz.zjc.util.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = {"工具类相关接口"})
@RestController
@EnableAutoConfiguration
public class UtilController {
    @ResponseBody
    @RequestMapping(value = "/getWeather", method = RequestMethod.GET)
    @ApiOperation(value="获取天气信息")
    public String getWeather(Integer type,Integer accountType,String account){

        return ResultUtil.fail("服务器异常，请稍后重试");
    }

}

package com.zz.zjc.controller;

import com.zz.zjc.model.Admin;
import com.zz.zjc.service.AdminService;
import com.zz.zjc.service.MenuService;
import com.zz.zjc.util.*;
import com.zz.zjc.vo.AdminVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Api(tags = {"管理员接口"})
@RestController
@EnableAutoConfiguration
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AdminService adminService;
    @Autowired
    MenuService menuService;

    /**
     * 登录
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ApiOperation(value = "登录")
    public String login(HttpSession session, String name, String pass) {
        if (ParamUtil.isNull(name) || ParamUtil.isNull(pass)) {
            return ResultUtil.failValidator("必要参数错误");
        }
        Admin admin = adminService.login(name, pass);
        if (admin == null) {
            return ResultUtil.fail("账号或者密码错误");
        } else {
            Integer state = admin.getState();
            if (state == 1) {
                RequestUtil.setAdmin(session, admin);
                return ResultUtil.success(admin, "登陆成功");
            } else {
                return ResultUtil.fail("账号关闭，登录失败，请联系管理员");
            }

        }
    }


    @ResponseBody
    @RequestMapping(value = "/loginOut", method = RequestMethod.GET)
    @ApiOperation(value = "退出登录")
    public String loginOut(HttpSession session) {
        RequestUtil.removeAdmin(session);
        return ResultUtil.success("退出登陆成功");
    }

    @ResponseBody
    @RequestMapping(value = "/getAdminInfo", method = RequestMethod.GET)
    @ApiOperation(value = "获取管理员信息")
    public String getAdminInfo(HttpSession session) {
        Admin admin = RequestUtil.getAdmin(session);
        if (admin == null) {
            return ResultUtil.failLoginInvalid();
        }
        Integer roleId = -1;
        try {
            roleId = admin.getRoleId() != null ? admin.getRoleId() : -1;
        } catch (Exception e) {
        }
        AdminVo adminVo = new AdminVo();
        BeanUtils.copyProperties(admin,adminVo);
        adminVo.setMenuList(menuService.getMenuListByRole(roleId));

        return ResultUtil.success(adminVo, "ok");
    }

    @ResponseBody
    @RequestMapping(value = "/getAdminList", method = RequestMethod.GET)
    @ApiOperation(value = "获取管理员列表")
    public String getAdminList(HttpSession session, String v) {
        HashMap map = new HashMap();
        if (!ParamUtil.isNull(v)) {
            map.put("v", "%" + v + "%");
        }
        return ResultUtil.success(adminService.getAdminList(map), "ok");
    }

    @ResponseBody
    @RequestMapping(value = "/updateRole", method = RequestMethod.POST)
    @ApiOperation(value = "批量修改管理员角色")
    public String updateRole(HttpSession session, Integer roleId, String ids) {
        String[] idArray = ids.split(",");
        List<Integer> idList = new ArrayList<>();
        for (int i = 0; i < idArray.length; i++) {
            try {
                idList.add(Integer.parseInt(idArray[i]));
            } catch (Exception e) {
            }
        }
        if (idList.size() > 0) {
            adminService.updateRole(roleId, idList);
            return ResultUtil.success("", "ok");
        } else {
            return ResultUtil.failValidator("参数错误");
        }
    }

    @ResponseBody
    @RequestMapping(value = "/saveAdmin", method = RequestMethod.POST)
    @ApiOperation(value = "保存管理员")
    public String saveMenu(Integer id, String name, String tel, Integer state, Integer roleId) {
        Admin admin = new Admin();
        if (id == null) {
            if (ParamUtil.isNull(name)) {
                return ResultUtil.failValidator("name不能为空");
            }
            if (ParamUtil.isNull(tel)) {
                return ResultUtil.failValidator("账号不能为空");
            }
            //随机6位字符串密码
            admin.setPass(NumberUtil.makeRandom_6());
        }
        admin.setId(id);
        admin.setName(name);
        admin.setTel(tel);
        admin.setState(state);
        admin.setRoleId(roleId == null ? 0 : roleId);
        adminService.saveAdmin(admin);
        return ResultUtil.success("保存成功", "");
    }


}

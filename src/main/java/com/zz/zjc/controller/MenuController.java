package com.zz.zjc.controller;

import com.zz.zjc.model.AdminRole;
import com.zz.zjc.model.Menu;
import com.zz.zjc.service.MenuService;
import com.zz.zjc.util.ParamUtil;
import com.zz.zjc.util.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.HashMap;

@Api(tags = {"后台菜单权限接口"})
@RestController
@EnableAutoConfiguration
@RequestMapping("/admin/menu")
public class MenuController {

    @Autowired
    MenuService menuService;

    @ResponseBody
    @RequestMapping(value = "/saveMenu", method = RequestMethod.POST)
    @ApiOperation(value = "保存菜单")
    public String saveMenu(Integer id, Integer pid, String name, String text, String value, String icon, Integer type) {
        Menu menu = new Menu();
        if (ParamUtil.isNull(name)) {
            return ResultUtil.failValidator("name不能为空");
        }
        menu.setId(id == -1 ? null : id);
        menu.setPid(pid == null ? -1 : pid);
        menu.setName(name);
        menu.setText(text);
        menu.setValue(value);
        menu.setIcon(icon);
        menu.setType(type);
        menuService.saveMenu(menu);
        return ResultUtil.success("保存成功", "");
    }

    @ResponseBody
    @RequestMapping(value = "/saveRole", method = RequestMethod.POST)
    @ApiOperation(value = "保存角色")
    public String saveRole(Integer id, String name, String menuList) {
        if (ParamUtil.isNull(name)) {
            return ResultUtil.failValidator("name不能为空");
        }
        AdminRole adminRole = new AdminRole();
        adminRole.setId(id == -1 ? null : id);
        adminRole.setName(name);
        adminRole.setMenuList(menuList);
        menuService.saveRole(adminRole);
        return ResultUtil.success("保存成功", "");
    }

    @ResponseBody
    @RequestMapping(value = "/getMenuList", method = RequestMethod.GET)
    @ApiOperation(value = "获取菜单信息")
    public String getMenuList(Integer type) {
        return ResultUtil.success(menuService.getMenuList(type), "");
    }

    @ResponseBody
    @RequestMapping(value = "/getMenuTree", method = RequestMethod.GET)
    @ApiOperation(value = "获取菜单树状列表")
    public String getMenuTree() {
        return ResultUtil.success(menuService.getMenuListByRole(-1), "");
    }

    @ResponseBody
    @RequestMapping(value = "/getRoleList", method = RequestMethod.GET)
    @ApiOperation(value = "获取角色信息")
    public String getRoleList() {
        return ResultUtil.success(menuService.getRoleList(), "");
    }
}

package com.zz.zjc.controller;

import com.zz.zjc.model.User;
import com.zz.zjc.service.UserService;
import com.zz.zjc.service.VerificationService;
import com.zz.zjc.util.ParamUtil;
import com.zz.zjc.util.RequestUtil;
import com.zz.zjc.util.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@Api(tags = {"用户相关接口"})
@RestController
@EnableAutoConfiguration
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    VerificationService verificationService;

    @ResponseBody
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ApiOperation(value = "登录")
    public String login(HttpSession session, String name, String pass) {
        if (ParamUtil.isNull(name) || ParamUtil.isNull(pass)) {
            return ResultUtil.failValidator("必要参数错误");
        }
        User user = userService.login(name, pass);
        if (user == null) {
            return ResultUtil.fail("账号或者密码错误");
        } else {
            Integer state = user.getState();
            if (state == 1) {
                RequestUtil.setUser(session, user);
                return ResultUtil.success(user, "登陆成功");
            } else {
                return ResultUtil.fail("账号关闭，登录失败");
            }

        }

    }

    @ResponseBody
    @RequestMapping(value = "/getUserInfo", method = RequestMethod.GET)
    @ApiOperation(value = "获取用户信息")
    public String getUserInfo(HttpSession session) {
        User user = RequestUtil.getUser(session);
        if (user == null) {
            return ResultUtil.failLoginInvalid();
        }
        return ResultUtil.success(user, "ok");
    }

    @ResponseBody
    @RequestMapping(value = "/outLogin", method = RequestMethod.GET)
    @ApiOperation(value = "退出登录")
    public String outLogin(HttpSession session) {
        RequestUtil.removeUser(session);
        return ResultUtil.success("", "以安全退出登陆");
    }


    @ResponseBody
    @RequestMapping(value = "/sendCode", method = RequestMethod.POST)
    @ApiOperation(value = "发送验证码 ")
    public String sendCode(Integer type, Integer accountType, String account) {
        int code = verificationService.makeCode(type, accountType, account);
        return ResultUtil.success("已发送注意查收," + code);
    }


    @ResponseBody
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ApiOperation(value = "注册")
    public String register(HttpSession session, Integer accountType, String account, String pass, String code) {
        int coder = userService.register(accountType, account, pass, code);
        if (coder == 200) {
            User user = userService.login(account, pass);
            if (user == null) {
                return ResultUtil.fail("服务器异常，请稍后重试");
            } else {
                RequestUtil.setUser(session, user);
                return ResultUtil.success("", "注册成功");
            }

        }
        return ResultUtil.fail("服务器异常，请稍后重试");
    }


    @ResponseBody
    @RequestMapping(value = "/getAdditionalUserInfo", method = RequestMethod.GET)
    @ApiOperation(value = "获取用户的补充信息")
    public String getAdditionalUserInfo(HttpSession session, Integer userId) {
        if (userId == null) {
            User user = RequestUtil.getUser(session);
            if (user == null) {
                return ResultUtil.fail("操作失败，参数错误");
            }
            userId = user.getId();
        }
        return ResultUtil.success(userService.get(userId), "");
    }


    @ResponseBody
    @RequestMapping(value = "/updateUserInfo", method = RequestMethod.POST)
    @ApiOperation(value = "修改用户信息")
    public String uploadHeadImage(HttpSession session, User user) {
        Integer userId = RequestUtil.getUserId(session);
        if (userId == null) {
            return ResultUtil.failLoginInvalid();
        }
        user.setId(userId);
        //限制非修改信息进入
        user.setAddTime(null);
        user.setMail(null);
        user.setTel(null);
        user.setExp(null);
        user.setGold(null);
        user.setLevel(null);
        user.setIntegral(null);
        user.setState(null);
        user.setPass(null);
        userService.update(user);
        //重新更新缓存的信息
        RequestUtil.setUser(session, userService.get(userId));
        return ResultUtil.success("");
    }

    @ResponseBody
    @RequestMapping(value = "/resetPass", method = RequestMethod.POST)
    @ApiOperation(value = "重置密码")
    public String resetPass(String account, String pass, String code) {
        userService.resetPass(account, pass, code);
        return ResultUtil.success("重置成功");
    }


}

package com.zz.zjc.controller;

import com.zz.zjc.service.ConfigService;
import com.zz.zjc.util.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = {"首页相关接口"})
@RestController
@EnableAutoConfiguration
public class IndexController {

    @Autowired
    private ConfigService configService;
    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ApiOperation(value="首页测试接口",notes="注意问题点")
    public String index (){
        return "欢迎来到zjc";
    }


    @ResponseBody
    @RequestMapping(value = "/getIndexBanner", method = RequestMethod.GET)
    @ApiOperation(value="获取首页轮播图")
    public String getIndexBanner (){
        return ResultUtil.success(configService.getIndexBanner(),"首页轮播图");
    }


    @ResponseBody
    @RequestMapping(value ="/test", method = RequestMethod.GET)
    @ApiOperation(value="异常测试")
    public String test(Integer t){
        throw new RuntimeException(t+"测试抛出异常");
    }

}

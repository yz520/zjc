package com.zz.zjc.service;

import com.zz.zjc.dao.UserDao;
import com.zz.zjc.dao.VerificationDao;
import com.zz.zjc.exception.MessageException;
import com.zz.zjc.model.User;
import com.zz.zjc.model.Verification;
import com.zz.zjc.util.AccountValidatorUtil;
import com.zz.zjc.util.MailboxUtil;
import com.zz.zjc.util.NumberUtil;
import com.zz.zjc.util.SMSUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class VerificationService {

    @Resource
    UserDao userDao;
    @Resource
    VerificationDao verificationDao;

    /**
     * 队列处理发送验证码
     *
     * @param verification
     */
    public void sendVerification(Verification verification) {
        //记录发送时间
        verification.setSendTime(new Date());
        if (verification.getAccountType() == Verification.accountTypeMail) {
            try {
                //发送邮件
                boolean ret = MailboxUtil.sendVerificationCode(verification.getName(), verification.getCode());
                if (ret) {
                    verification.setState(Verification.stateSendOut);
                } else {
                    verification.setState(Verification.stateFail);
                }
            } catch (Exception e) {
                verification.setState(Verification.stateFail);
            }
            verificationDao.updateByPrimaryKeySelective(verification);

        } else if (verification.getAccountType() == Verification.accountTypeTel) {
            try {
                int code = SMSUtil.sendVerificationCode(verification.getName(), verification.getCode());
                if (code == SMSUtil.CODE_OK) {
                    verification.setState(Verification.stateSendOut);
                } else {
                    verification.setState(Verification.stateFail);
                }
            } catch (Exception e) {
                verification.setState(Verification.stateFail);
            }
            verificationDao.updateByPrimaryKeySelective(verification);
        }
    }

    /**
     * 生成验证码
     *
     * @param type
     * @param accountType
     * @param account
     * @return
     */
    public int makeCode(Integer type, Integer accountType, String account) {
        List<User> userList = userDao.queryAccount(account);
        Verification verification = new Verification();
        verification.setAccountType(accountType);
        verification.setName(account);
        verification.setType(type);
        verification.setState(Verification.stateWait);
        verification.setAddTime(new Date());
        if (type == 0 && userList.size() > 0) {
            //账号已经注册
            throw new MessageException("账号已经注册");
        }
        List<Verification> verificationList = verificationDao.queryVerification(account, type, 5 * 60);
        if (verificationList.size() > 2) {
            throw new MessageException("5分钟内最多发送3次");
        }
        String code = NumberUtil.makeRandom_6();
        verification.setCode(code);
        if (accountType == 0) {
            if (!AccountValidatorUtil.isEmail(account)) {
                //邮箱格式不支持，请联系管理员
                throw new MessageException("邮箱格式不支持，请联系管理员");
            }
        } else if (accountType == 1) {
            if (!AccountValidatorUtil.isMobile(account)) {
                throw new MessageException("手机号格式不支持，请联系管理员");
            }
        }
        Integer id = verificationDao.insertSelective(verification);
        verification.setId(id);
        sendVerification(verification);
        return Integer.parseInt(code);
    }

    /**
     * 校验验证码的合法性
     *
     * @param type
     * @param account
     * @param code
     * @return
     */
    public int checkCode(Integer type, String account, String code) {
        List<Verification> verificationList = verificationDao.queryVerification(account, type, 60 * 5);
        if (verificationList.size() > 0) {
            Verification verification = verificationList.get(0);
            if (verification.getCode().equals(code)) {
                //
                verificationDao.updateState(verification.getId(),Verification.stateUse);
                return 200;
            } else {
                //验证码错误
                throw new MessageException("验证码错误");
            }
        } else {
            //验证码过期
            throw new MessageException("验证码过期");
        }

    }
}

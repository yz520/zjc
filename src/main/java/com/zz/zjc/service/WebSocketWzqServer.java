package com.zz.zjc.service;

import com.alibaba.fastjson.JSONObject;
import com.zz.zjc.model.Player;
import com.zz.zjc.model.User;
import com.zz.zjc.util.SpringContextUtil;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@ServerEndpoint("/webSocketWzq/{sid}")
@Component
public class WebSocketWzqServer {


    static UserService userService = null;


    //静态变量，用来记录当前在线连接数。应该把它设计成线程安全的。
    private static AtomicInteger onlineNum = new AtomicInteger();

    //concurrent包的线程安全Set，用来存放每个客户端对应的WebSocketServer对象。
    private static ConcurrentHashMap<String, Session> sessionPools = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<String, Player> playPools = new ConcurrentHashMap<>();

    //发送消息
    public void sendMessage(Session session, String message) throws IOException {
        if (session != null) {
            synchronized (session) {
                System.out.println("发送数据：" + message);
                session.getBasicRemote().sendText(message);
            }
        }
    }

    //给指定用户发送信息
    public void sendInfo(String id, String message) {
        Session session = sessionPools.get(id);
        try {
            sendMessage(session, message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loginOut(String id) {
        Session session = sessionPools.get(id);
        if (session != null) {
            try {
                sendMessage(session, getJsonStr("msg", "xitong", "<span style= \"color:#0000FF;\">【账号在其他地方登录】</span>"));
                session.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void sendInfoAll(String message) {
        for (Session session : sessionPools.values()) {
            try {
                sendMessage(session, message);
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
        }
    }

    //建立连接成功调用
    @OnOpen
    public void onOpen(Session session, @PathParam(value = "sid") String sid) {
        //先判断 用户真实  过滤假用户
        if (userService == null) {
            userService = (UserService) SpringContextUtil.getBean("userService");
        }
        if(onlineNum.get()>=200){
            try {
                sendMessage(session, getJsonStr("msg", "xitong", "<span style= \"color:#0000FF;\">【当前人数过多，请稍后再试】</span>"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        User user = userService.get(Integer.parseInt(sid));
        if (user != null) {
            loginOut(sid);
            sessionPools.put(sid, session);
            addOnlineCount();
            System.out.println(sid + "加入webSocket！当前人数为" + onlineNum);
            Player player = new Player();
            player.setId(sid);
            player.setName(user.getName());
            player.setHead(user.getHead());
            playPools.put(sid, player);
        } else {
            try {
                sendMessage(session, getJsonStr("msg", "xitong", "<span style= \"color:#0000FF;\">【你是个假用户】</span>"));
                session.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    //关闭连接时调用
    @OnClose
    public void onClose(@PathParam(value = "sid") String sid) {
        sessionPools.remove(sid);
        playPools.remove(sid);
        subOnlineCount();
        System.out.println(sid + "断开webSocket连接！当前人数为" + onlineNum);
        sendInfoAll(getJsonStr("onlineNum", "", playPools));
    }

    //收到客户端信息
    @OnMessage
    public void onMessage(String dataJson) throws IOException {
        System.out.println("收到数据：" + dataJson);
        try {
            JSONObject data = JSONObject.parseObject(dataJson);
            sendInfoAll(data.toJSONString());
        } catch (Exception e) {
            e.printStackTrace();

        }
        //
        //

    }

    //错误时调用
    @OnError
    public void onError(Session session, Throwable throwable) {
        System.out.println("发生错误");
        throwable.printStackTrace();
    }

    public static void addOnlineCount() {
        onlineNum.incrementAndGet();
    }

    public static void subOnlineCount() {
        onlineNum.decrementAndGet();
    }

    public static String getJsonStr(String type, String sid, Object data) {
        JSONObject json = new JSONObject();
        json.put("type", type);
        json.put("sid", sid);
        json.put("data", data);
        return json.toString();
    }




}
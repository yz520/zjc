package com.zz.zjc.service;

import com.zz.zjc.dao.AdminRoleDao;
import com.zz.zjc.dao.MenuDao;
import com.zz.zjc.model.AdminRole;
import com.zz.zjc.model.Menu;
import com.zz.zjc.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class MenuService {

    @Autowired
    MenuDao menuDao;

    @Resource
    AdminRoleDao adminRoleDao;

    @Cacheable(value = "menuList")
    public List<Menu> getMenuList(Integer type) {
        return menuDao.getMenuList(type);
    }

    public List<Menu> getMenuListByRole(Integer roleId) {
        List<Menu> list = new ArrayList<>();
        List<Menu> allList = menuDao.getMenuList(1);
        List<Menu> listAll = new ArrayList<>();
        for (int i = 0; i < allList.size(); i++) {
            listAll.add(allList.get(i));
        }
        List<Menu> tree = getTreeMenu(listAll, null, -1);
        if (roleId == -1) {
            return tree;
        }
        AdminRole role = adminRoleDao.selectByPrimaryKey(roleId);
        if (role == null) {
            return list;
        }
        String menuListIds = role.getMenuList();

        if (!menuListIds.equals("") && !menuListIds.equals("null")) {
            list = getMyTreeMenu(tree, menuListIds);
        }
        return list;
    }

    List<Menu> getMyTreeMenu(List<Menu> list, String ids) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getList().size() > 0) {
                list.get(i).setList(getMyTreeMenu(list.get(i).getList(), ids));
                list.get(i).setFlag(isIds(ids, list.get(i).getId()));
                if (!list.get(i).isFlag()) {
                    list.get(i).setFlag(list.get(i).getList().size() > 0);
                }

            } else {
                list.get(i).setFlag(isIds(ids, list.get(i).getId()));
            }
            if (!list.get(i).isFlag()) {
                list.remove(i);
                i--;
            }
        }
        return list;
    }

    public static boolean isIds(String ids, Integer id) {
        if (ids == null || id == null) {
            return false;
        }
        return ("," + ids + ",").contains("," + id + ",");
    }

    List<Menu> getTreeMenu(List<Menu> list, List<Menu> tree, Integer pid) {
        if (tree == null) {
            tree = new ArrayList<>();
        }
        for (int i = 0; i < list.size(); i++) {
            if (pid.equals(list.get(i).getPid())) {
                list.get(i).setList(getTreeMenu(list, null, list.get(i).getId()));
                tree.add(list.get(i));
            }
        }
        return tree;
    }

    public List<AdminRole> getRoleList() {
        return adminRoleDao.getRoleList();
    }

    public void saveMenu(Menu menu) {
        if (menu.getId() == null) {
            menuDao.insertSelective(menu);
        } else {
            menuDao.updateByPrimaryKeySelective(menu);
        }
    }

    public void saveRole(AdminRole adminRole) {
        if (adminRole.getId() == null) {
            adminRoleDao.insertSelective(adminRole);
        } else {
            adminRoleDao.updateByPrimaryKeySelective(adminRole);
        }

    }
}

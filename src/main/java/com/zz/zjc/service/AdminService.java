package com.zz.zjc.service;

import com.zz.zjc.dao.AdminDao;
import com.zz.zjc.model.Admin;
import com.zz.zjc.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class AdminService {

    @Autowired
    AdminDao adminDao;


    public Admin login(String name, String pass) {
        return adminDao.login(name, pass);
    }

    public Admin getAdminInfo(Integer id) {
        return adminDao.selectByPrimaryKey(id);
    }

    public List<Admin> getAdminList(HashMap<String, Object> map) {
        return adminDao.getAdminList(map);
    }

    public void updateRole(Integer roleId, List<Integer> ids) {
        adminDao.updateRole(roleId, ids);
    }

    public void saveAdmin(Admin admin) {
        if (admin.getId() == null) {
            adminDao.insertSelective(admin);
        } else {
            adminDao.updateByPrimaryKeySelective(admin);
        }
    }
}

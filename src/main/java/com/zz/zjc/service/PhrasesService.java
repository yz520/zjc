package com.zz.zjc.service;

import com.zz.zjc.dao.PhrasesDao;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author 诸杰成
 * @description
 * @date 2020/7/23 17:02
 */
@Service
public class PhrasesService {

    @Resource
    PhrasesDao phrasesDao;

    public long check(String name) {
        return phrasesDao.check(name);
    }


    public String find(String text, String pinyin) {
        return phrasesDao.find(text, pinyin);
    }

    public String rand() {
        return phrasesDao.rand();
    }


}

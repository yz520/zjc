package com.zz.zjc.service;

import com.zz.zjc.dao.ConfigDao;
import com.zz.zjc.model.Config;
import com.zz.zjc.service.ConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class ConfigService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Resource
    private RedisTemplate<String, Object> redisTemplate;
    @Autowired
    ConfigDao configDao;


    public List<Config> getIndexBanner() {
        String key = "getIndexBanner";
        boolean hasKey = redisTemplate.hasKey(key);
        if (hasKey) {
            List<Config> object = (List<Config>) redisTemplate.opsForValue().get(key);
            return object;
        }
        List<Config> object = configDao.getCinfigListByKey("indexBannerImage");
        redisTemplate.opsForValue().set(key, object, 3, TimeUnit.DAYS);
        return object;
    }


}

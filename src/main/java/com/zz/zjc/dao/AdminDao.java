package com.zz.zjc.dao;

import com.zz.zjc.model.Admin;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.HashMap;
import java.util.List;

public interface AdminDao extends Mapper<Admin> {

    /**
     * 管理员登陆
     * @param tel
     * @param pass
     * @return
     */
    Admin login(@Param("arg0") String tel, @Param("arg1") String pass);

    /**
     * 查询管理员列表
     * @param map
     * @return
     */
    List<Admin> getAdminList(HashMap<String,Object> map);

    /**
     * P批量修改管理员角色
     * @param roleId
     * @param ids
     */
    void updateRole(@Param("roleId") Integer roleId, @Param("ids") List<Integer> ids);




}

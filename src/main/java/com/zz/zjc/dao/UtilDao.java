package com.zz.zjc.dao;

import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

public interface UtilDao {
    List<HashMap> getColumnsListByName(@Param("name")String name);
}

package com.zz.zjc.dao;

import com.zz.zjc.model.AdminRole;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * @author 诸杰成
 * @description
 * @date 2020/4/8 15:10
 */
public interface AdminRoleDao extends Mapper<AdminRole> {

    List<AdminRole> getRoleList();
}

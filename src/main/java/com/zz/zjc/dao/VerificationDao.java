package com.zz.zjc.dao;

import com.zz.zjc.model.Verification;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface VerificationDao extends Mapper<Verification> {

    /**
     * 查找最近一段时间的验证码
     * @param account 账号
     * @param type 验证码类型
     * @param effective 有效时间
     * @return
     */
    List<Verification> queryVerification(@Param("account")String account,@Param("type")int type,@Param("effective")long effective);

    /**
     *
     * @param id
     * @param state
     */
    void updateState(@Param("id")int id,@Param("state")int state);

}

package com.zz.zjc.dao;

import com.zz.zjc.model.Phrases;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author 诸杰成
 * @description
 * @date 2020/7/23 16:36
 */
public interface PhrasesDao extends Mapper<Phrases> {

    long check(@Param("name") String name);

    String find(@Param("text") String text, @Param("pinyin") String pinyin);

    String rand();
}

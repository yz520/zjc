package com.zz.zjc.dao;

import com.zz.zjc.model.News;
import com.zz.zjc.vo.NewsVo;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;


public interface NewsDao extends Mapper<News> {

    List<NewsVo> getList();
}

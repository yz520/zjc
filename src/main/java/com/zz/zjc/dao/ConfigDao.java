package com.zz.zjc.dao;

import com.zz.zjc.model.Config;
import org.apache.ibatis.annotations.Param;


import java.util.List;

public interface ConfigDao {

    List<Config> getCinfigListByKey(@Param("arg0")String key);

    Config getCinfigByKey(@Param("arg0")String key);
}

package com.zz.zjc.dao;

import com.zz.zjc.model.User;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;


import java.util.List;

public interface UserDao extends Mapper<User> {

    /**
     * 登陆
     * @param account
     * @param pass
     * @return
     */
    User login(@Param("account")String account,@Param("pass")String pass);




    /**
     * 查询
     * @param account
     * @return
     */
    List<User> queryAccount(@Param("account")String account);


}

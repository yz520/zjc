package com.zz.zjc.dao;

import org.apache.ibatis.annotations.Param;

import com.zz.zjc.model.NewsType;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface NewsTypeDao extends Mapper<NewsType> {

    List<NewsType> getList(@Param("pid") Integer pid);

    List<NewsType> getListAll();
}

package com.zz.zjc.config;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @author 诸杰成
 * @description
 * @date 2020/4/8 16:52
 */
public class BaseModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4362521055868561994L;


    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}

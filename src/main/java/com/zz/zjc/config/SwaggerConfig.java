package com.zz.zjc.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * apidoc配置
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                //swagger要扫描的包路径
                .apis(RequestHandlerSelectors.basePackage("com.zz.zjc.controller"))
                .paths(PathSelectors.any())
                .build();
    }
    @Value("${swagger.host}")
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("[ZJC]项目  API 文档")
                .description("包含前后端所有的接口API文档"
                        +"返回数据事例：{\"msg\":\"ok\",\"code\":200,\"data\":{}}"
                        +"<br />"
                        +"注意code（int） 和 msg(String)一定存在，data 要看具体API接口决定"
                        +"<br />"
                        +"返回状态码说明："
                        +"<br />"
                        +"200：成功"
                        +"<br />"
                        +"其他：错误（例如：参数校验不通过，服务器异常，未查询到数据）"
                        +"")
                //.termsOfServiceUrl("http://api.zhujiecheng.top/zjc")
                //.contact(new Contact("管理员",null,null))
                .version("1.0")
                .build();
    }


}

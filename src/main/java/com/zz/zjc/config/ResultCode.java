package com.zz.zjc.config;

public enum ResultCode {
    CODE_OK(200,"正常状态"),

    CODE_ERROR_MESSAGE_EXCEPTION(999,"自定义消息异常"),

    CODE_ERROR(1000,"系统错误"),
    CODE_ERROR_EMPTY(1001,"查询为空"),
    CODE_ERROR_VALIDATOR(1002,"参数校验不通过"),
    CODE_ERROR_VALIDATOR_LOGIN(1003,"登录失效"),
    ;
    private int code;
    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    ResultCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}

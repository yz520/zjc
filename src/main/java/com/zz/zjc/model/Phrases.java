package com.zz.zjc.model;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author 诸杰成
 * @description
 * @date 2020/7/25 13:35
 */
@Table(name = "phrases")
public class Phrases implements Serializable {

    @Id
    private Integer phraseid;

    private String chengyu;

    /*private String pingyin;

    private String diangu;

    private String chuchu;

    private String lizi;

    private String spellfirst;*/

    private String pingyinfirst;

    private String hanzifirst;

    private String pingyinfirst2;

    public Integer getPhraseid() {
        return phraseid;
    }

    public void setPhraseid(Integer phraseid) {
        this.phraseid = phraseid;
    }

    public String getChengyu() {
        return chengyu;
    }

    public void setChengyu(String chengyu) {
        this.chengyu = chengyu;
    }

    /*public String getPingyin() {
        return pingyin;
    }

    public void setPingyin(String pingyin) {
        this.pingyin = pingyin;
    }

    public String getDiangu() {
        return diangu;
    }

    public void setDiangu(String diangu) {
        this.diangu = diangu;
    }

    public String getChuchu() {
        return chuchu;
    }

    public void setChuchu(String chuchu) {
        this.chuchu = chuchu;
    }

    public String getLizi() {
        return lizi;
    }

    public void setLizi(String lizi) {
        this.lizi = lizi;
    }

    public String getSpellfirst() {
        return spellfirst;
    }

    public void setSpellfirst(String spellfirst) {
        this.spellfirst = spellfirst;
    }*/

    public String getPingyinfirst() {
        return pingyinfirst;
    }

    public void setPingyinfirst(String pingyinfirst) {
        this.pingyinfirst = pingyinfirst;
    }

    public String getHanzifirst() {
        return hanzifirst;
    }

    public void setHanzifirst(String hanzifirst) {
        this.hanzifirst = hanzifirst;
    }

    public String getPingyinfirst2() {
        return pingyinfirst2;
    }

    public void setPingyinfirst2(String pingyinfirst2) {
        this.pingyinfirst2 = pingyinfirst2;
    }
}

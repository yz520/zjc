package com.zz.zjc.model;

import java.io.Serializable;

/**
 * @author 诸杰成
 * @description
 * @date 2020/7/23 15:36
 */
public class Player implements Serializable {

    private String id;

    private String name;

    private String head;

    private Integer gold;

    private Integer isLine;

    private Integer isHave = 1;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public Integer getGold() {
        return gold;
    }

    public void setGold(Integer gold) {
        this.gold = gold;
    }

    public Integer getIsLine() {
        return isLine;
    }

    public void setIsLine(Integer isLine) {
        this.isLine = isLine;
    }

    public Integer getIsHave() {
        return isHave;
    }

    public void setIsHave(Integer isHave) {
        this.isHave = isHave;
    }

    public Player() {
    }

    public Player(Integer isHave) {
        this.isHave = isHave;
    }
}

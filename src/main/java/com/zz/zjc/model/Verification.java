package com.zz.zjc.model;

import com.zz.zjc.config.BaseModel;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Table(name = "verification")
public class Verification extends BaseModel {

    /**
     * 手机类型，发短信
     */
    final public static Integer accountTypeTel = 1;
    /**
     * 邮箱类型，发邮件
     */
    final public static Integer accountTypeMail = 0;

    /**
     * 待发送
     */
    final public static Integer stateWait = 1;
    /**
     * 已发送
     */
    final public static Integer stateSendOut = 2;
    /**
     * 发生失败
     */
    final public static Integer stateFail = -1;
    /**
     * 已使用
     */
    final public static Integer stateUse = -2;

    private String code;
    private String name;
    private Date addTime;
    private Date sendTime;
    private Integer type;//验证码类型， //type  0 注册验证码   1 重置密码验证码
    private Integer accountType;//账号类型
    private Integer state;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getSendTime() {
        return sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    public Integer getAccountType() {
        return accountType;
    }

    public void setAccountType(Integer accountType) {
        this.accountType = accountType;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

}

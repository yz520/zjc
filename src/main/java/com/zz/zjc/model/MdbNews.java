package com.zz.zjc.model;

import javax.persistence.Id;
import java.io.Serializable;

public class MdbNews implements Serializable {
    @Id
    String id;
    String text;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }

}

package com.zz.zjc.model;


import com.zz.zjc.config.BaseModel;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "news_type")
public class NewsType extends BaseModel {

    private Integer pid;
    private String name;
    private String text;
    private Integer state;

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}

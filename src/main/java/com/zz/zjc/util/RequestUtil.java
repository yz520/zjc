package com.zz.zjc.util;

import com.zz.zjc.model.Admin;
import com.zz.zjc.model.User;

import javax.servlet.http.HttpSession;
import java.util.HashMap;

public class RequestUtil {

    public static String key_user = "user";
    public static String key_user_id = "user_id";
    public static String key_admin = "admin";
    public static String key_admin_id = "admin_id";

    public static void setUser(HttpSession session, User user) {
        if (user != null) {
            session.setAttribute(key_user_id, user.getId());
            session.setAttribute(key_user, user);
        }
    }

    public static void removeUser(HttpSession session) {
        session.removeAttribute(key_user);
        session.removeAttribute(key_user_id);
    }

    public static User getUser(HttpSession session) {
        User user = (User) session.getAttribute(key_user);
        return user;
    }

    public static Integer getUserId(HttpSession session) {
        Object userId = session.getAttribute(key_user_id);
        if (userId != null) {
            return Integer.parseInt(userId + "");
        }
        return null;
    }

    public static void setAdmin(HttpSession session, Admin admin) {
        if (admin != null) {
            session.setAttribute(key_admin_id, admin.getId());
            session.setAttribute(key_admin, admin);
        }
    }

    public static void removeAdmin(HttpSession session) {
        session.removeAttribute(key_admin);
        session.removeAttribute(key_admin_id);
    }

    public static Admin getAdmin(HttpSession session) {
        Admin admin = (Admin) session.getAttribute(key_admin);
        return admin;
    }

    public static Integer getAdminId(HttpSession session) {
        Object adminId = session.getAttribute(key_admin_id);
        if (adminId != null) {
            return Integer.parseInt(adminId + "");
        }
        return null;
    }


}

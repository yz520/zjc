package com.zz.zjc.util;

import com.alibaba.fastjson.JSONObject;
import com.zz.zjc.config.ResultCode;

public class ResultUtil {

    /**
     * 默认返回自定义
     * @param data
     * @param code
     * @return
     */
    public static String custom(Object data,ResultCode code) {
        JSONObject json= new JSONObject();
        json.put("code", code.getCode());
        json.put("msg", code.getMsg());
        json.put("data", data);
        return json.toString();
    }
    public static String custom(Object data,String msg,int code) {
        JSONObject json= new JSONObject();
        json.put("code", code);
        json.put("msg", msg);
        json.put("data", data);
        return json.toString();
    }

    /**
     * 返回成功带数据和消息
     * @param object
     * @param msg
     * @return
     */
    public static String success(Object object,String msg) {
        return custom(object,ResultCode.CODE_OK);
    }

    /**
     * 返回成功带数据
     * @param object
     * @return
     */
    public static String success(Object object) {
        return success(object,"ok");
    }

    /**
     * 返回成功
     * @return
     */
    public static String success() {
        return success("","ok");
    }
    /**
     * 返回失败带状态
     * @param code
     * @return
     */
    public static String fail(ResultCode code) {
        return custom("",code);
    }

    /**
     * 返回失败带状态和信息
     * @param code
     * @param msg
     * @return
     */
    public static String fail(int code,String msg) {
        return custom("",msg,code);
    }
    /**
     * 返回失败带信息
     * @param msgs
     * @return
     */
    public static String fail(String... msgs) {
        if (msgs.length>0){
            return fail(ResultCode.CODE_ERROR.getCode(),msgs[0]);
        }else {
            return fail(ResultCode.CODE_ERROR);
        }
    }

    /**
     * 参数校验不通过
     * @param msgs
     * @return
     */
    public static String failValidator(String... msgs) {
        if (msgs.length>0){
            return fail(ResultCode.CODE_ERROR_VALIDATOR.getCode(),msgs[0]);
        }else {
            return fail(ResultCode.CODE_ERROR_VALIDATOR);
        }

    }

    /**
     * 返回空数据
     * @param msgs
     * @return
     */
    public static String failEmpty(String... msgs){
        if (msgs.length>0){
            return fail(ResultCode.CODE_ERROR_EMPTY.getCode(),msgs[0]);
        }else {
            return fail(ResultCode.CODE_ERROR_EMPTY);
        }
    }

    /**
     * 登录失效
     * @return
     */
    public static String failLoginInvalid() {
        return fail(ResultCode.CODE_ERROR_VALIDATOR_LOGIN);
    }
}

package com.zz.zjc.util;

public class ParamUtil {
    /**
     * 判断为空
     * @param obj
     * @return
     */
    public static boolean isNull(Object obj){
        if (null==obj){
            return true;
        }
        if("".equals(obj)||"null".equals(obj)){
            return true;
        }
        return false;
    }
}

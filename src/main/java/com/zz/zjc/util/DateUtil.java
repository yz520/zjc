package com.zz.zjc.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    /**
     * 默认格式字符串转换时间 yyyy-MM-dd hh:mm:ss
     * @param str
     * @return
     */
    public static Date changeDate(String str){
        try {
            return sdf.parse(str);
        }catch (Exception e){}

        return null;
    };
    /**
     * 指定 sdf 格式字符串转换时间
     * @param str
     * @return
     */
    public static Date changeDate(String str,String sdf){
        try {
            return new SimpleDateFormat(sdf).parse(str);
        }catch (Exception e){}

        return null;
    };

    /**
     * 时间转换默认字符串  yyyy-MM-dd hh:mm:ss
     * @param date
     * @return
     */
    public static String changeDate(Date date){
        if(date==null){
            return null;
        }
        try {
            return sdf.format(date);
        }catch (Exception e){

        }
        return null;
    };

    /**
     * 时间转换自定义格式字符串   sdf
     * @param date
     * @return
     */
    public static String changeDate(Date date,String sdf){
        if(date==null){
            return null;
        }
        try {
            return new SimpleDateFormat(sdf).format(date);
        }catch (Exception e){

        }
        return null;
    };
}
